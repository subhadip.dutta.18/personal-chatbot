Create conda environment

conda create --name rasaxenv python=3.7

conda activate rasaxenv

pip install rasa==2.8.2

pip install rasa-x==0.39.3 --extra-index-url https://pypi.rasa.com/simple

pip3 install SQLAlchemy==1.3.22

pip install sanic-jwt==1.6.0

1)Create rasa project files

rasa init

2)For new branch

git init

3)Clone project folders to local

git clone https://gitlab.com/subhadip.dutta.18/personal-chatbot.git

4)Create new branch

git checkout -b dev

5)Add all changes

git add .

6)Commit new changes to the branch

git Commit
(Add comments)
Esc->:wq!

7)Push to origin branch in repository from local changes

git push --set-upstream origin dev

For rasa webchat

rasa = 2.3.4
python 3.7.5
pip install python-socketio==4.6.1
pip install python-engineio==3.13.2
rasa-webchat 1.0.0

For running rasa

# Instructions

*Make sure you have set up the Rasa on your system or server.*

## Integration

- Step 1: Since this Chat UI communicates with the Rasa server using `rest` channel, make sure you have added `rest` channel in the `credentials.yml` file
- Once you have developed your bot and you are ready to integrate the bot with the UI, you can start the Rasa server using the below command
  ```
  rasa run -m models --enable-api --cors "*" --debug
  ```
- If you have custom actions, you can start the action server using the below command
    ```
    rasa run actions --cors "*" --debug
    ```
- Once you have you Rasa server up and running, you can test the bot by running the `index.html` file in the browser.


