# This files contains your custom actions which can be used to run
# custom Python code.
#
# See this guide on how to implement these action:
# https://rasa.com/docs/rasa/custom-actions


# This is a simple example for a custom action which utters "Hello World!"

from typing import Any, Text, Dict, List

from rasa_sdk import Action, Tracker, FormValidationAction
from rasa_sdk.executor import CollectingDispatcher
from rasa_sdk.events import SlotSet,UserUtteranceReverted
import os.path
import requests
import json
import smtplib
import pymongo
from pymongo import MongoClient

def read_token():
    with open("token.txt", "r") as f:
        lines = f.readlines()
        mongopas = lines[0].strip()
        email_user = lines[1].strip()
        email_pass = lines[2].strip()
        gen_key = lines[3].strip()
        subred_cl_id = lines[4].strip()
        subred_cl_token = lines[5].strip()
        apikeyweathr = lines[6].strip()
        apiimdbkey = lines[7].strip()
        authlist = [mongopas,email_user,email_pass,gen_key,subred_cl_id,subred_cl_token,apikeyweathr,apiimdbkey]
        return authlist


mongopass = read_token()[0]
smtp_username = read_token()[1]
smtp_password = read_token()[2]
genius_key = read_token()[3]
red_client_id = read_token()[4]
red_client_token = read_token()[5]
api_key_weather = read_token()[6]
imdb_key = read_token()[7]

#MongoDB
mongourl = f"mongodb+srv://AIGAMEDEV:{mongopass}@bethcluster.pc6ir.mongodb.net/Rasa_Personal_Assistant_DB?retryWrites=true&w=majority"
cluster = MongoClient(mongourl)
db = cluster["Rasa_Personal_Assistant_DB"]


class ActionHelloWorld(Action):

    def name(self) -> Text:
        return "action_hello_world"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        dispatcher.utter_message(text="Hello World!")

        return []

class ActionValidateUser(Action):

    def name(self) -> Text:
        return "action_validate_user"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        slots = []
        result = []
        name_contact = tracker.get_slot('name')
        email_contact = tracker.get_slot('email')
        print(f"Contact Name: {name_contact} \n Contact Email: {email_contact}")
        collectioncontact = db["Contact"]
        querycontact = {"email": email_contact}
        contactinfo = collectioncontact.find_one(querycontact)
        if contactinfo is not None:
            name_contact = contactinfo["name"]
            dispatcher.utter_message(text=f"Thank you for providing your info.How can we help you {name_contact}?")
            result = [SlotSet("name", name_contact),SlotSet("contact_present",True)]
        else:
            dispatcher.utter_message(text=f"Sorry we could'nt find any interaction with the provided email.")

        return result


class ActionRegisterContact(Action):

    def name(self) -> Text:
        return "action_register_contact"

    def run(self, dispatcher: CollectingDispatcher,
            tracker: Tracker,
            domain: Dict[Text, Any]) -> List[Dict[Text, Any]]:

        slots = []
        result = []
        name_contact = tracker.get_slot('name')
        email_contact = tracker.get_slot('email')
        print(f"Contact Name: {name_contact} \n Contact Email: {email_contact}")
        collectioncontact = db["Contact"]
        querycontact = {"email": email_contact}
        contactinfo = collectioncontact.find_one(querycontact)
        if contactinfo is not None:
            name_contact = contactinfo["name"]
            dispatcher.utter_message(text=f"Thank you for providing your info.It seems I have interacted with you before.Nice to talk to you again.How can I help you {name_contact}?")
        else:
            newvalues = {
                            "name": name_contact,
                            "email":email_contact
                        }
            try:
                collectioncontact.insert_one(newvalues)
                dispatcher.utter_message(text=f"It is nice to make your acquaintance.How can I help you {name_contact}?")
                result = [SlotSet("contact_present",True)]
            except Exception as e:
                print(str(e))            

        return result

